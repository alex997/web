let path = require('path');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
let autoprefixer = require('autoprefixer');
let cssnano = require('cssnano');
let HtmlWebpackPlugin = require('html-webpack-plugin');
let CopyWebpackPlugin = require('copy-webpack-plugin');


let conf = {
    entry: {use:['./src/index.js', './src/styles.scss']},
    output: {
    	path: path.resolve(__dirname, './dist'),
    	filename: 'main.js',
    	publicPath: 'dist/'
    },
    devServer:{
    	overlay: true
    },
     module:{
    	rules:[
    	{
    		test:/\.js$/,
    		loader:'babel-loader',
    		//exclude:'/node_modules/'
    	},
    	{
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
                loader: 'css-loader',
                options: {
                    // If you are having trouble with urls not resolving add this setting.
                    // See https://github.com/webpack-contrib/css-loader#url
                    url: false,
                    minimize: true,
                    sourceMap: true
                }
            }, 

               {
                    loader: 'postcss-loader',
                    options: {
                        plugins: [
                            autoprefixer({
                                browsers:['ie >= 8', 'last 4 version']
                            }),
                            cssnano({

                            })
                        ],
                        sourceMap: true
                     }
                },
            {
                loader: 'sass-loader',
                options: {
                    sourceMap: true
                }
            }
          ]
        })
      }    
    	]
    },
    plugins: [
     new ExtractTextPlugin('style.css'),
     new HtmlWebpackPlugin({
      inject: false,
      hash: true,
      template: './src/index.html',
      filename: 'index.html'
     }),
   new CopyWebpackPlugin([
      { from: './src/img/', to: './img/' }
    ],
      {
        debug: true,
        ignore: [ '*.js', '*.css', '*.scss' ]
      }),
    ]	,
   

    
};
module.exports = conf;